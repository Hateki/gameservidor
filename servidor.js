var express = require('express');
var app = express();
const path = require('path');
const PORT = process.env.PORT || 3000;
const INDEX = path.join(__dirname, 'web-desktop/index.html');

app.use(express.static("web-desktop"));
app.use((req, res) => {
    console.log("O Index é = " + INDEX);
    res.sendFile(INDEX);
});

app.listen(PORT, function () {
    console.log('App de Exemplo escutando na porta 3000!');
});